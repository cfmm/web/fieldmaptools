import os
import setuptools

setuptools.setup(
    name="fieldmaptools",
    version=os.getenv("CI_COMMIT_TAG") or "0.1",
    author="Alan Kuurstra",
    author_email="akuurstr@uwo.ca",
    description="Tools for accessing and processing CFMM fieldmaps.",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cfmm/web/fieldmaptools",
    packages=setuptools.find_packages(),
    install_requires=[
        "requests>=2.25.1",
        "numpy>=1.19.1",
        "h5py>=3.1.0",
        "lxml>=4.6.2",
        "python-dateutil>=2.8.1",
        "libang @ git+https://gitlab.com/Kuurstra/libang.git@758e3dc0e3184806b102ccb5a94e2daf63e0926a",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires=">=3.8",
)
