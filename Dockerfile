# pin alpine version to avoid hdf5 1.14.4 bug (https://github.com/h5py/h5py/issues/2419)
FROM python:3.12.4-alpine3.19 AS base

RUN --mount=type=cache,target=/var/cache/apk apk add --quiet --no-cache hdf5 hdf5-hl openblas libstdc++

FROM base AS compiler

RUN --mount=type=cache,target=/var/cache/apk apk add --quiet git gcc g++ hdf5-dev make openblas-dev linux-headers

RUN python -m venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH" \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1

RUN --mount=type=cache,target=/root/.cache/pip pip install h5py scikit-image

COPY requirements.txt ./
RUN --mount=type=cache,target=/root/.cache/pip pip install black flake8 pytest pytest-mock -r requirements.txt

FROM base AS runner

COPY --from=compiler /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH" \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1
