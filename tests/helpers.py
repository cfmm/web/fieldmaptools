import datetime
import logging

import h5py
import numpy as np


def setup_logger():
    logger = logging.getLogger("FieldmapsClient")
    if not len(logger.handlers):
        console_handler = logging.StreamHandler()
        format = logging.Formatter(
            "%(asctime)s - %(filename)s - %(lineno)d - %(name)s - %(levelname)s - %(message)s",
            "%Y-%m-%d:%H:%M:%S",
        )
        console_handler.setLevel(logging.DEBUG)
        console_handler.setFormatter(format)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(console_handler)
    return logger


def create_test_b0_file(fp, generated_datetime_str=None):
    if generated_datetime_str is None:
        datetime_now = datetime.datetime.now().astimezone()
        generated_datetime_str = datetime_now.strftime("%Y-%m-%d %H:%M:%S %Z")
        generated_datetime_str = generated_datetime_str.replace(
            "EDT", "Eastern Daylight Time"
        ).replace("EST", "Eastern Standard Time")

    f = h5py.File(fp, "w")
    ds = f.create_dataset("/images/xml", 1, dtype=h5py.string_dtype(encoding="utf-8"))
    ds[0] = (
        f'<?xml version=\'1.0\' encoding=\'UTF-8\'?>\n<ismrmrdHeader xmlns="http://www.ismrm.org/ISMRMRD" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" xsi:schemaLocation="http://www.ismrm.org/ISMRMRD ismrmrd.xsd">\n<subjectInformation>\n<patientLOID>testPatientLOID</patientLOID>\n<patientName>testPatientName</patientName>\n<patientWeight_kg>90.718483</patientWeight_kg>\n<patientSex>2</patientSex>\n<patientHeight>1828.803711</patientHeight>\n<patientAge>30.000000</patientAge>\n<studyLOID>testStudyLOID</studyLOID>\n<studyDescription/>\n</subjectInformation>\n<acquisitionSystemInformation>\n<systemVendor>SIEMENS</systemVendor>\n<systemModel>testSystem</systemModel>\n<systemSoftwareVersions>syngo MR E12</systemSoftwareVersions>\n<systemSerialNumber>18932</systemSerialNumber>\n<institutionName>University of Western Ontario</institutionName>\n<systemFieldStrength_T>6.980936</systemFieldStrength_T>\n<receiverChannels>32</receiverChannels>\n<relativeReceiverNoiseBandwidth>0.79</relativeReceiverNoiseBandwidth>\n</acquisitionSystemInformation>\n<experimentalConditions>\n<ProcessingDateTime>{generated_datetime_str}</ProcessingDateTime>\n<H1resonanceFrequency_Hz>297227073</H1resonanceFrequency_Hz>\n<TransmittingCoil>testCoil</TransmittingCoil>\n<ProtocolName>tfl_rfmap</ProtocolName>\n<SequenceFileName>%SiemensSeq%\\tfl_rfmap</SequenceFileName>\n<ScanningSequence>GR</ScanningSequence>\n<SequenceVariant>SK\\SP</SequenceVariant>\n<SiemensSequence>1</SiemensSequence>\n<SequenceString>tfl2d1_16</SequenceString>\n<FlipAngleDegree>5.000000</FlipAngleDegree>\n<txScaleFactors>\n<txScaleFactor>\n<dRe>0.353600</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>0.245600</dRe>\n<dIm>-0.254300</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>-0.061400</dRe>\n<dIm>0.348200</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>0.338100</dRe>\n<dIm>0.103400</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>0.187400</dRe>\n<dIm>-0.299800</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>-0.330100</dRe>\n<dIm>-0.126700</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>0.339900</dRe>\n<dIm>-0.097500</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>-0.049200</dRe>\n<dIm>-0.350100</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n<txScaleFactor>\n<dRe>1.000000</dRe>\n<dIm>0.000000</dIm>\n</txScaleFactor>\n</txScaleFactors>\n<Nuclei>\n<Nucleus>\n<referenceAmplitude>480.798889</referenceAmplitude>\n</Nucleus>\n<Nucleus>\n<referenceAmplitude>0.000000</referenceAmplitude>\n</Nucleus>\n</Nuclei>\n</experimentalConditions>\n<encoding>\n<trajectory>cartesian</trajectory>\n<encodedSpace>\n<matrixSize>\n<x>128</x>\n<y>64</y>\n<z>1</z>\n</matrixSize>\n<fieldOfView_mm>\n<x>512</x>\n<y>256</y>\n<z>5</z>\n</fieldOfView_mm>\n</encodedSpace>\n<reconSpace>\n<matrixSize>\n<x>64</x>\n<y>64</y>\n<z>1</z>\n</matrixSize>\n<fieldOfView_mm>\n<x>256.000000</x>\n<y>256.000000</y>\n<z>5.000000</z>\n</fieldOfView_mm>\n</reconSpace>\n<encodingLimits>\n<kspace_encoding_step_1>\n<minimum>0</minimum>\n<maximum>63</maximum>\n<center>32</center>\n</kspace_encoding_step_1>\n<kspace_encoding_step_2>\n<minimum>0</minimum>\n<maximum>0</maximum>\n<center>0</center>\n</kspace_encoding_step_2>\n<slice>\n<minimum>0</minimum>\n<maximum>24</maximum>\n<center>0</center>\n</slice>\n<set>\n<minimum>0</minimum>\n<maximum>0</maximum>\n<center>0</center>\n</set>\n</encodingLimits>\n</encoding>\n<parallelImaging>\n<accelerationFactor>\n<kspace_encoding_step_1>2</kspace_encoding_step_1>\n<kspace_encoding_step_2>1</kspace_encoding_step_2>\n</accelerationFactor>\n<calibrationMode>embedded</calibrationMode>\n</parallelImaging>\n<sequenceParameters>\n<TR>4050</TR>\n<TE>1.56</TE>\n<TI>300</TI>\n<TI>1000</TI>\n</sequenceParameters>\n</ismrmrdHeader>'  # noqa: E501
    )

    ds = f.create_dataset(
        "/images/Raw Data/attributes", 1, dtype=h5py.string_dtype(encoding="utf-8")
    )
    ds[0] = (
        '<?xml version="1.0"?>\n<ismrmrdMeta>\n\t<meta>\n\t\t<name>Image Type</name>\n\t\t<value>Raw Data</value>\n\t</meta>\n\t<meta>\n\t\t<name>Map Type</name>\n\t\t<value>B0</value>\n\t</meta>\n</ismrmrdMeta>\n'  # noqa: E501
    )

    raw_data_datatype = np.dtype([("real", "<f4"), ("imag", "<f4")])
    raw_data_array = np.ones(shape=(72, 1, 1, 64, 64), dtype=raw_data_datatype)
    f.create_dataset("/images/Raw Data/data", data=raw_data_array)
    return f


def create_test_b1_file(fp, generated_datetime_str=None):
    f = h5py.File(fp, "w")
    ds = f.create_dataset("/images/xml", 1, dtype=h5py.string_dtype(encoding="utf-8"))
    with open("b1_IsmrmrdHeader.xml", mode="rb") as xml_input_file:
        ds[0] = xml_input_file.read()
    # search and replace siemens/custom/DateTime with a generated time if future tests require it
    return f
