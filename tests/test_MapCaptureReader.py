import datetime
import tempfile

import h5py
import lxml.etree as ET
import numpy as np
import pytest

from fieldmaptools.exceptions import BadFile, IncompleteData, IncorrectXML
from fieldmaptools.mapcapturereader import MapCaptureReader, xml_getter, xml_setter
from tests.helpers import setup_logger, create_test_b0_file, create_test_b1_file

logger = setup_logger()

datetime_now = datetime.datetime.now().astimezone()
generated_datetime_str = datetime_now.strftime("%Y-%m-%d %H:%M:%S %Z")
generated_datetime_obj = datetime.datetime.strptime(
    generated_datetime_str, "%Y-%m-%d %H:%M:%S %Z"
).astimezone()
generated_datetime_str = generated_datetime_str.replace("EDT", "Eastern Daylight Time")
generated_datetime_str = generated_datetime_str.replace("EST", "Eastern Standard Time")


@pytest.fixture
def h5_b0_file():
    with tempfile.NamedTemporaryFile(delete=True) as fp:
        f = create_test_b0_file(fp, generated_datetime_str)
        f.close()
        yield fp.name


@pytest.fixture
def h5_b1_file():
    with tempfile.NamedTemporaryFile(delete=True) as fp:
        f = create_test_b1_file(fp, generated_datetime_str)
        f.close()
        yield fp.name


@pytest.fixture
def h5_file_yes_data_no_xml():
    with tempfile.NamedTemporaryFile(delete=True) as fp:
        f = create_test_b0_file(fp, generated_datetime_str)
        del f["images"]["xml"]
        f.close()
        yield fp.name


@pytest.fixture
def h5_file_no_data():
    with tempfile.NamedTemporaryFile(delete=True) as fp:
        f = create_test_b0_file(fp, generated_datetime_str)
        del f["images"]["Raw Data"]
        f.close()
        yield fp.name


def test_getter_file_doesnt_exist():
    reader = MapCaptureReader("abc", "r")
    with pytest.raises(AttributeError):
        reader.patientLOID


def test_getter_bad_datetime(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r+")
    xml_setter(reader, "abc", [("ProcessingDateTime", True)])
    with pytest.raises(AttributeError):
        reader.generatedDateTime


def test_getter_no_xml(h5_file_yes_data_no_xml):
    reader = MapCaptureReader(h5_file_yes_data_no_xml, "r")
    with pytest.raises(AttributeError):
        reader.patientLOID


def test_getter_bad_xml_tag(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r+")
    with pytest.raises(AttributeError):
        xml_getter(reader, [("abc", True)])


def run_test_xml_setter(h5_file, attribute, value, xml_element_name=None):
    if xml_element_name is None:
        xml_element_name = attribute
    reader = MapCaptureReader(h5_file, "r+")
    setattr(reader, attribute, value)
    reader.close()
    xml_obj = ET.XML(h5py.File(h5_file)["images"]["xml"][0])
    assert xml_obj.findall(f".//*{xml_element_name}", xml_obj.nsmap)[0].text == value


def test_patientLOID_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.patientLOID == "testPatientLOID"


def test_patientLOID_setter(h5_b0_file):
    run_test_xml_setter(h5_b0_file, "patientLOID", "abcd")


def test_patientName_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.patientName == "testPatientName"


def test_patientName_setter(h5_b0_file):
    run_test_xml_setter(h5_b0_file, "patientName", "abcd")


def test_studyLOID_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.studyLOID == "testStudyLOID"


def test_studyLOID_setter(h5_b0_file):
    run_test_xml_setter(h5_b0_file, "studyLOID", "abcd")


def test_systemModel_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.systemModel == "testSystem"


def test_systemModel_setter(h5_b0_file):
    run_test_xml_setter(h5_b0_file, "systemModel", "abcd")


def test_transmittingCoil_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.transmittingCoil == "testCoil"


def test_transmittingCoil_setter(h5_b0_file):
    run_test_xml_setter(h5_b0_file, "transmittingCoil", "abcd", "TransmittingCoil")


def test_generatedDateTime_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.generatedDateTime == generated_datetime_obj


def test_generatedDateTime_setter(h5_b0_file):
    new_time_obj = datetime.datetime.now().astimezone()

    reader = MapCaptureReader(h5_b0_file, "r+")
    setattr(reader, "generatedDateTime", new_time_obj)
    reader.close()

    xml_obj = ET.XML(h5py.File(h5_b0_file)["images"]["xml"][0])
    results = xml_obj.findall(".//*ProcessingDateTime", xml_obj.nsmap)
    assert results[0].text == new_time_obj.isoformat(sep=" ")


def test_rawData_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    data = reader._rawData_native
    assert np.array_equal(
        data, np.ones(shape=data.shape) + 1j * np.ones(shape=data.shape)
    )


def test_maptype_getter(h5_b0_file):
    reader = MapCaptureReader(h5_b0_file, "r")
    assert reader.maptypes == {"b0"}


def test_file_does_not_exist():
    reader = MapCaptureReader("asdfkl", "r")
    with pytest.raises(BadFile):
        reader.check_file()


def test_yes_data_no_xml(h5_file_yes_data_no_xml):
    reader = MapCaptureReader(h5_file_yes_data_no_xml, "r")
    with pytest.raises(IncorrectXML):
        reader.check_file()


def test_no_data(h5_file_no_data):
    reader = MapCaptureReader(h5_file_no_data, "r")
    with pytest.raises(IncompleteData):
        reader.check_file()


def test_zero_data():
    with tempfile.NamedTemporaryFile(delete=True) as fp:
        f = create_test_b0_file(fp, generated_datetime_str)
        del f["images"]["Raw Data"]
        f.create_dataset(
            "/images/Raw Data/data",
            data=np.zeros(
                shape=(72, 1, 1, 64, 64),
                dtype=np.dtype([("real", "<f4"), ("imag", "<f4")]),
            ),
        )
        f.close()
        reader = MapCaptureReader(fp.name, "r")
        with pytest.raises(IncompleteData):
            reader.check_file()


def test_new_xml(h5_b1_file):
    r = MapCaptureReader(h5_b1_file, "r")
    print(r.generatedDateTime)
    print(r.studyLOID)
    print(r.patientLOID)
    print(r.patientName)
    print(r.transmittingCoil)
    print(r.systemModel)
