import datetime
import json
import tempfile

import pytest
import requests

from fieldmaptools.fieldmapsclient import (
    FieldmapsClient,
    fieldmap_filetypes,
    taggable_models,
)
from fieldmaptools.helpers import Tag, Op, OpBase
from fieldmaptools.server_models import B1
from tests.helpers import setup_logger

logger = setup_logger()


class Tester(object):
    __test__ = False

    def __init__(self, host, port, secure, auth):
        self.host = host
        self.port = port
        self.secure = secure
        self.auth = auth
        self.client = FieldmapsClient(self.root, self.auth)

    @property
    def root(self):
        return f"http{'s' if self.secure else ''}://{self.host}:{str(self.port)}"


@pytest.fixture
def get_mocker(mocker):
    return mocker.patch("fieldmaptools.fieldmapsclient.requests.get")


@pytest.fixture
def post_mocker(mocker):
    return mocker.patch("fieldmaptools.fieldmapsclient.requests.post")


permute_servers = [
    Tester("testHost", 443, secure=True, auth="1234"),
    Tester("testHost", 443, secure=True, auth="abcd"),
    Tester("testHost", 443, secure=False, auth="1234"),
    Tester("testHost", 5000, secure=True, auth="1234"),
    Tester("newHost", 443, secure=True, auth="1234"),
]
model_keys = [1, 200]
permute_upload_path_system_coil = [
    ("test.h5", "testSystem", "testCoil"),
    ("new.h5", "testSystem", "testCoil"),
    ("test.h5", "new", "testCoil"),
    ("test.h5", "testSystem", "new"),
]


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_download(tester, get_mocker, imgtype, imgid):
    tester.client.get(imgtype, imgid)
    get_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/{imgid}", headers={"Authorization": tester.auth}
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_info(tester, get_mocker, imgtype, imgid):
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.info(imgtype, imgid)
    get_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/{imgid}/info",
        headers={"Authorization": tester.auth},
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_listafter(tester, get_mocker, imgtype, imgid):
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.list(imgtype, imgid)
    get_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/list/{imgid}",
        headers={"Authorization": tester.auth},
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_list(tester, get_mocker, imgtype, imgid):
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.list(imgtype)
    get_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/list", headers={"Authorization": tester.auth}
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_count(tester, get_mocker, imgtype, imgid):
    tester.client.count(imgtype)
    get_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/count", headers={"Authorization": tester.auth}
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("path,system,coil", permute_upload_path_system_coil)
def test_register(tester, post_mocker, imgtype, path, system, coil):
    tester.client.register(imgtype, path, system, coil)
    post_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/register",
        headers={"Authorization": tester.auth, "Content-Type": "application/json"},
        data=f'{{"path": "{path}", "system": "{system}", "coilname": "{coil}"}}'.encode(
            "utf-8"
        ),
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("path,system,coil", permute_upload_path_system_coil)
def test_upload(tester, post_mocker, imgtype, path, system, coil):
    # path not used, extra tests computed
    with tempfile.NamedTemporaryFile(mode="w") as tmp:
        with open(tmp.name, "rb") as filehandle:
            tester.client.upload(imgtype, filehandle, system, coil)
            post_mocker.assert_called_once_with(
                tester.root + f"/api/{imgtype}/upload",
                headers={
                    "Authorization": tester.auth,
                    "coilname": coil,
                    "system": system,
                },
                data=filehandle,
            )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_modify_file(tester, post_mocker, imgtype, imgid):
    # path not used, extra tests computed
    with tempfile.NamedTemporaryFile(mode="w") as tmp:
        with open(tmp.name, "rb") as filehandle:
            tester.client.modify_file(imgtype, imgid, filehandle)
            post_mocker.assert_called_once_with(
                tester.root + f"/api/{imgtype}/{imgid}/modify_file",
                headers={
                    "Authorization": tester.auth,
                },
                data=filehandle,
            )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("imgtype", fieldmap_filetypes)
@pytest.mark.parametrize("imgid", model_keys)
def test_classify(tester, post_mocker, imgtype, imgid):
    # path not used, extra tests computed
    classname = "human"
    tester.client.classify(imgtype, imgid, classname)
    post_mocker.assert_called_once_with(
        tester.root + f"/api/{imgtype}/{imgid}/classify",
        headers={
            "Authorization": tester.auth,
        },
        data=f'{{"subject": "{classname}"}}'.encode("utf-8"),
    )


def test_save(mocker):
    mocked_response = requests.Response()
    mocked_response.status_code = 200
    mocked_response._content = "Test Contents".encode("utf-8")
    client_get_mocker = mocker.patch(
        "fieldmaptools.fieldmapsclient.FieldmapsClient.get",
        return_value=mocked_response,
    )
    tmp = tempfile.NamedTemporaryFile()
    tester = Tester("testHost", 443, secure=True, auth="1234")
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    with pytest.raises(ValueError):
        tester.client.save(B1.__api_name__, 1, tmp.name, overwrite=False)
    client_get_mocker.assert_not_called()
    tester.client.save(B1.__api_name__, 1, tmp.name, overwrite=True)
    with open(tmp.name, "rb") as f:
        assert f.read() == mocked_response.content


@pytest.mark.parametrize("tag_prefix", ["", "un"])
@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("model", taggable_models)
@pytest.mark.parametrize("model_key", model_keys)
def test_tag_untag(tester, post_mocker, model, model_key, tag_prefix):
    tag = "test_tag"
    if tag_prefix:
        tester.client.untag(model, model_key, tag)
    else:
        tester.client.tag(model, model_key, tag)
    data = json.dumps({"tags": [tag]}).encode("utf-8")

    post_mocker.assert_called_once_with(
        tester.root + f"/api/{model}/{model_key}/{tag_prefix}tag",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tag_prefix", ["", "un"])
def test_tag_untag_list(post_mocker, tag_prefix):
    tags = ["test_tag1", "test_tag2"]

    model = "b0"
    model_key = 1
    tester = Tester("testHost", 443, secure=True, auth="1234")
    if tag_prefix:
        tester.client.untag(model, model_key, tags)
    else:
        tester.client.tag(model, model_key, tags)
    data = json.dumps({"tags": tags}).encode("utf-8")
    post_mocker.assert_called_once_with(
        tester.root + f"/api/{model}/{model_key}/{tag_prefix}tag",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
def test_tag_delete(tester, post_mocker):
    tags = ["test_tag1", "test_tag2"]
    tester.client.delete_tags(tags)
    data = json.dumps({"tags": tags}).encode("utf-8")
    post_mocker.assert_called_once_with(
        f"{tester.root}/api/tag/delete",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
def test_tag_rename(tester, post_mocker):
    tags = ["test_tag1", "test_tag2"]
    tester.client.delete_tags(tags)
    data = json.dumps({"tags": tags}).encode("utf-8")
    post_mocker.assert_called_once_with(
        f"{tester.root}/api/tag/delete",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
def test_tag_merge(tester, post_mocker):
    tags = ["test_tag1", "test_tag2"]
    merge_name = "test_tag"
    tester.client.merge_tags(tags, merge_name)
    data = json.dumps({"tags": tags, "merged_name": merge_name}).encode("utf-8")
    post_mocker.assert_called_once_with(
        f"{tester.root}/api/tag/merge",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
def test_tag_list_similar(tester, post_mocker):
    tags = ("test", 100)
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.list_similar_tags(tags)
    data = json.dumps({"tags": [{"name": tags[0], "threshold": tags[1]}]}).encode(
        "utf-8"
    )
    post_mocker.assert_called_once_with(
        f"{tester.root}/api/tag/similar",
        headers={"Authorization": tester.auth},
        data=data,
    )


def test_tag_list_similar_multiple(post_mocker):
    tags = [("test", 100), ("test2",), "test3"]
    tester = Tester("testHost", 443, secure=True, auth="1234")
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.list_similar_tags(tags)
    data = json.dumps(
        {
            "tags": [
                {"name": tags[0][0], "threshold": tags[0][1]},
                {"name": tags[1][0]},
                {"name": tags[2]},
            ]
        }
    ).encode("utf-8")
    post_mocker.assert_called_once_with(
        f"{tester.root}/api/tag/similar",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("model", taggable_models)
def test_query_tag(post_mocker, tester, model):
    tag1 = "tag1"
    tag2 = "tag2"
    tags = [Tag(tag1), Tag(tag2, op=Op(OpBase.and_))]
    # rather than create mockers with the correct http response, just ignore the response and ensure
    # the http function call is correct
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.query(model, tags=tags)
    payload = {"tags": [{"tag": tag.json_dict} for tag in tags]}
    data = json.dumps(payload).encode("utf-8")
    post_mocker.assert_called_once_with(
        tester.root + f"/api/{model}/query",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("model", taggable_models)
def test_query_coil_id(post_mocker, tester, model):
    # rather than create mockers with the correct http response, just ignore the http response
    # and ensure the http function call is correct
    coil_ids = [1, 2]
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.query(model, coil_ids=coil_ids)
    payload = {"coils": coil_ids}
    data = json.dumps(payload).encode("utf-8")
    post_mocker.assert_called_once_with(
        tester.root + f"/api/{model}/query",
        headers={"Authorization": tester.auth},
        data=data,
    )


@pytest.mark.parametrize("tester", permute_servers)
@pytest.mark.parametrize("model", taggable_models)
def test_query_generated(post_mocker, tester, model):
    # rather than create mockers with the correct http response, just ignore the http response
    # and ensure the http function call is correct

    before = datetime.datetime.now()
    after = before - datetime.timedelta(days=5)
    generated_range = (after, before)
    with pytest.raises(ValueError):
        tester.client.query(model, generated=generated_range)

    before = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    after = before - datetime.timedelta(days=5)
    generated_range = (after, before)
    # TypeError occurs because query() tries to json deserialize to value returned by _http_post(), but
    # that function is mocked and does not return anything. Since we only care about the values given
    # to the mocked function, we ignore the error regarding an incorrect http response.
    with pytest.raises(TypeError):
        tester.client.query(model, generated=generated_range)
    payload = {"generated": (after.isoformat(), before.isoformat())}
    data = json.dumps(payload).encode("utf-8")
    post_mocker.assert_called_once_with(
        tester.root + f"/api/{model}/query",
        headers={"Authorization": tester.auth},
        data=data,
    )
