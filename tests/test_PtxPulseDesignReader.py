import datetime
import tempfile

import h5py
import numpy as np
import pytest

from fieldmaptools.exceptions import BadFile
from fieldmaptools.ptxpulsedesignreader import PtxPulseDesignReader
from tests.helpers import setup_logger

logger = setup_logger()

datetime_now = datetime.datetime.now().astimezone()
generated_datetime_str = datetime_now.strftime("%Y-%m-%d %H:%M:%S %z")
generated_datetime_str = f"{generated_datetime_str[:-2]}:{generated_datetime_str[-2:]}"
generated_datetime_obj = datetime.datetime.fromisoformat(generated_datetime_str)


@pytest.fixture
def h5_file():
    with tempfile.NamedTemporaryFile(delete=True) as fp:
        f = h5py.File(fp, "w")
        h5py_string_dtype = h5py.string_dtype(encoding="utf-8")
        patientName_ascii_array = np.array(
            [
                [116.0],
                [101.0],
                [115.0],
                [116.0],
                [80.0],
                [97.0],
                [116.0],
                [105.0],
                [101.0],
                [110.0],
                [116.0],
                [78.0],
                [97.0],
                [109.0],
                [101.0],
            ],
            dtype="<f8",
        )
        f.create_dataset(
            "PatientInfo",
            shape=1,
            dtype=[
                ("MainLoid", h5py_string_dtype),
                (
                    "Name",
                    np.dtype(
                        (patientName_ascii_array.dtype, patientName_ascii_array.shape)
                    ),
                ),
                ("Time", h5py_string_dtype),
                ("StudyLOID", h5py_string_dtype),
            ],
        )
        record_scalar = f["PatientInfo"][0]
        record_scalar["MainLoid"] = "testPatientLOID"
        record_scalar["Name"] = patientName_ascii_array
        record_scalar["Time"] = generated_datetime_str
        record_scalar["StudyLOID"] = "testStudyLOID"
        f["PatientInfo"][0] = record_scalar

        f.create_dataset(
            "CoilInfo",
            1,
            dtype=[("Name", h5py_string_dtype), ("SystemName", h5py_string_dtype)],
        )
        record_scalar = f["CoilInfo"][0]
        record_scalar["Name"] = "testCoil"
        record_scalar["SystemName"] = "testSystem"
        f["CoilInfo"][0] = record_scalar

        f.close()
        yield fp.name


def run_test_ptxpulsedesign_setter(
    h5_file, attribute, value, h5_datasetname, h5_fieldname, decode_h5pystring=False
):
    reader = PtxPulseDesignReader(h5_file, "r+")
    setattr(reader, attribute, value)
    reader.close()

    dataset = h5py.File(h5_file)[h5_datasetname]
    if decode_h5pystring:
        encoding = h5py.check_string_dtype(dataset[h5_fieldname].dtype).encoding
        assigned_value = dataset[h5_fieldname][0].decode(encoding)
    else:
        assigned_value = dataset[h5_fieldname][0]
    assert assigned_value == value


def test_patientLOID_getter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r")
    assert reader.patientLOID == "testPatientLOID"


def test_patientLOID_setter(h5_file):
    run_test_ptxpulsedesign_setter(
        h5_file,
        "patientLOID",
        "abcd",
        "PatientInfo",
        "MainLoid",
        decode_h5pystring=True,
    )


def test_patientName_getter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r")
    assert reader.patientName == "testPatientName"


def test_patientName_setter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r+")
    new_value = "abcd"
    setattr(reader, "patientName", new_value)
    reader.close()

    assigned_name_encoded = h5py.File(h5_file, "r")["PatientInfo"]["Name"][0]
    assigned_name_decoded = "".join([chr(int(x[0])) for x in assigned_name_encoded])
    assert assigned_name_decoded == new_value


def test_studyLOID_getter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r")
    assert reader.studyLOID == "testStudyLOID"


def test_studyLOID_setter(h5_file):
    run_test_ptxpulsedesign_setter(
        h5_file, "studyLOID", "abcd", "PatientInfo", "StudyLOID", decode_h5pystring=True
    )


def test_systemModel_getter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r")
    assert reader.systemModel == "testSystem"


def test_systemModel_setter(h5_file):
    run_test_ptxpulsedesign_setter(
        h5_file, "systemModel", "abcd", "CoilInfo", "SystemName", decode_h5pystring=True
    )


def test_transmittingCoil_getter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r")
    assert reader.transmittingCoil == "testCoil"


def test_transmittingCoil_setter(h5_file):
    run_test_ptxpulsedesign_setter(
        h5_file, "transmittingCoil", "abcd", "CoilInfo", "Name", decode_h5pystring=True
    )


def test_generatedDateTime_getter(h5_file):
    reader = PtxPulseDesignReader(h5_file, "r")
    assert reader.generatedDateTime == generated_datetime_obj


def test_generatedDateTime_setter(h5_file):
    new_time_obj = datetime.datetime.now().astimezone()

    reader = PtxPulseDesignReader(h5_file, "r+")
    setattr(reader, "generatedDateTime", new_time_obj)
    reader.close()

    assigned_time = h5py.File(h5_file)["PatientInfo"]["Time"]
    encoding = h5py.check_string_dtype(assigned_time.dtype).encoding
    assigned_time_string = assigned_time[0].decode(encoding)

    assert assigned_time_string == new_time_obj.isoformat(sep=" ")


def test_file_does_not_exist():
    reader = PtxPulseDesignReader("asdfkl", "r")
    with pytest.raises(BadFile):
        reader.check_file()
