import datetime

import h5py
import numpy as np
import re

from .fieldmapreader import FieldmapReader


def get_field(self, dataset_name, fieldname):
    try:
        field = self._get_hdf5_path([dataset_name, fieldname])
    except ValueError:
        raise AttributeError(
            f"dataset {dataset_name}'s field {fieldname} not found in PtxPulseDesign file."
        )
    return field


def hdf5_getter(self, dataset_name, fieldname, decode_h5pystring=False):
    """
    :param name:
        Name of the dataset (absolute or relative).
    :return:
    """
    field = get_field(self, dataset_name, fieldname)
    if decode_h5pystring:
        encoding = h5py.check_string_dtype(field.dtype).encoding
        return field[0].decode(encoding)
    return field[0]


def hdf5_setter(self, value, dataset_name, fieldname):
    # throw error if field doesn't exist
    get_field(self, dataset_name, fieldname)

    # https://forum.hdfgroup.org/t/setting-dataset-string-field-results-in-typeerror/8256
    # https://github.com/h5py/h5py/issues/1857
    # field[0] = value

    # this assumes value matches record's original dtypes including each dtype's subarray shape if applicable
    # note: shape is immutable for numeric arrays but not for h5py strings
    record_scalar = self._hdf5[dataset_name][0]
    fieldname_index = self._hdf5[dataset_name].dtype.names.index(fieldname)
    record_scalar[fieldname_index] = value
    self._hdf5[dataset_name][0] = record_scalar


def hdf5_setter_change_dtype(self, value, dataset_name, fieldname):
    # throw error if field doesn't exist
    get_field(self, dataset_name, fieldname)

    # change subarray dtype shape
    record_scalar = self._hdf5[dataset_name][0]
    new_dtype_dict = {
        name: dtype_offset_tuple[0]
        for name, dtype_offset_tuple in self._hdf5[dataset_name].dtype.fields.items()
    }
    new_dtype_dict[fieldname] = np.dtype(
        (new_dtype_dict[fieldname].subdtype[0], value.shape)
    )
    new_dtype = list(new_dtype_dict.items())
    new_dataset_data = np.empty(shape=1, dtype=new_dtype)
    for element, element_fieldname in zip(record_scalar, new_dtype_dict.keys()):
        if element_fieldname == fieldname:
            element = value
        new_dataset_data[element_fieldname] = element
    del self._hdf5[dataset_name]
    self._hdf5.create_dataset(dataset_name, dtype=new_dtype, data=new_dataset_data)


def make_hdf5_property(
    dataset_name, fieldname, decode_h5pystring=False, change_dtype_shape=False
):
    def getter(self):
        return hdf5_getter(self, dataset_name, fieldname, decode_h5pystring)

    if change_dtype_shape:

        def setter(self, value):
            hdf5_setter_change_dtype(self, value, dataset_name, fieldname)

    else:

        def setter(self, value):
            hdf5_setter(self, value, dataset_name, fieldname)

    return property(getter, setter)


class PtxPulseDesignReader(FieldmapReader):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def generatedDateTime(self):
        date_string = hdf5_getter(self, "PatientInfo", "Time", decode_h5pystring=True)
        return datetime.datetime.fromisoformat(date_string)

    @generatedDateTime.setter
    def generatedDateTime(self, value):
        if value.utcoffset() is None:
            value = value.astimezone()
        value = value.isoformat(sep=" ")
        hdf5_setter(self, value, "PatientInfo", "Time")

    patientLOID = make_hdf5_property("PatientInfo", "MainLoid", decode_h5pystring=True)

    @property
    def patientName(self):
        name_encoded = hdf5_getter(self, "PatientInfo", "Name")
        # decode ASCII
        return "".join([chr(int(x[0])) for x in name_encoded])

    @patientName.setter
    def patientName(self, value):
        name_decoded = [[ord(x)] for x in value]
        value = np.asarray(name_decoded, dtype="<f8")
        hdf5_setter_change_dtype(self, value, "PatientInfo", "Name")

    studyLOID = make_hdf5_property("PatientInfo", "StudyLOID", decode_h5pystring=True)

    systemModel = make_hdf5_property("CoilInfo", "SystemName", decode_h5pystring=True)

    transmittingCoil = make_hdf5_property("CoilInfo", "Name", decode_h5pystring=True)

    @property
    def protocolName(self):
        return re.search(
            r"\ntProtocolName\t = \t\"(.+?)\"\n",
            self._hdf5["MrProt"][0][0].decode("utf-8"),
        ).group(1)

    def create_hdf5(self):
        f = h5py.File(self.filepath, "w")
        h5py_string_dtype = h5py.string_dtype(encoding="utf-8")
        generated_datetime_str = datetime.datetime.now().astimezone().isoformat(sep=" ")
        f.create_dataset(
            "PatientInfo",
            shape=1,
            data=np.array(
                ("", np.array([[0]], dtype="<f8"), generated_datetime_str, ""),
                dtype=[
                    ("MainLoid", h5py_string_dtype),
                    ("Name", np.dtype(("<f8", (1,)))),
                    ("Time", h5py_string_dtype),
                    ("StudyLOID", h5py_string_dtype),
                ],
            ),
        )
        f.create_dataset(
            "CoilInfo",
            1,
            dtype=[("Name", h5py_string_dtype), ("SystemName", h5py_string_dtype)],
        )
        f.create_dataset(
            "MrProt",
            1,
            dtype=[("ProtStr", h5py_string_dtype)],
        )
        f.create_dataset(
            "Adj",
            1,
        )
        f.close()
        # clear the cached hdf5 so the file can be reloaded
        try:
            del self._hdf5
        except AttributeError:
            pass

    @property
    def fieldmap(self):
        try:
            # every time you access the h5py Dataset's getter, it returns a new copy of the array
            # which does not affect the original dataset version
            return self._get_hdf5_path(["Adj", 0, "B0"]).transpose(1, 2, 0)
        except AttributeError as err:
            raise AttributeError(f"fieldmap: {str(err)}")

    @property
    def fov(self):
        fov = np.array(
            [
                self._get_hdf5_path(["Adj", 0, "values_n"]).max()
                - self._get_hdf5_path(["Adj", 0, "values_n"]).min(),
                self._get_hdf5_path(["Adj", 0, "values_m"]).max()
                - self._get_hdf5_path(["Adj", 0, "values_m"]).min(),
                self._get_hdf5_path(["Adj", 0, "values_s"]).max()
                - self._get_hdf5_path(["Adj", 0, "values_s"]).min(),
            ]
        ) + np.array(
            [
                self._get_hdf5_path(["Adj", 0, "values_n", 1, 0])
                - self._get_hdf5_path(["Adj", 0, "values_n", 0, 0]),
                self._get_hdf5_path(["Adj", 0, "values_m", 1, 0])
                - self._get_hdf5_path(["Adj", 0, "values_m", 0, 0]),
                self._get_hdf5_path(["Adj", 0, "values_s", 1, 0])
                - self._get_hdf5_path(["Adj", 0, "values_s", 0, 0]),
            ]
        )

        return fov

    @property
    def center(self):
        return np.array(
            [
                (
                    self._get_hdf5_path(["Adj", 0, "values_n", -1, 0])
                    + self._get_hdf5_path(["Adj", 0, "values_n", 0, 0])
                )
                / 2.0,
                (
                    self._get_hdf5_path(["Adj", 0, "values_m", -1, 0])
                    + self._get_hdf5_path(["Adj", 0, "values_m", 0, 0])
                )
                / 2.0,
                (
                    self._get_hdf5_path(["Adj", 0, "values_s", -1, 0])
                    + self._get_hdf5_path(["Adj", 0, "values_s", 0, 0])
                )
                / 2.0,
            ]
        )
