import datetime
import json
import logging
import sys
from pathlib import Path
from typing import Union, List, Dict, Tuple, Sequence, Any, BinaryIO, Optional

import dateutil.parser
import numpy as np
import requests

from .helpers import Tag
from .server_models import get_models, B0, B1, PTx

logger = logging.getLogger(__name__)


def _exception_handler(type_, value, tb):
    if tb.tb_frame.f_globals["__file__"] == __file__:
        logger.exception(f"Uncaught exception: {value}")
    raise value


sys.excepthook = _exception_handler

fieldmap_filetypes = [m.__api_name__ for m in get_models(is_filetype=True)]
taggable_models = [m.__api_name__ for m in get_models(is_taggable=True)]


class FieldmapsClient:
    """Client for interacting with a Fieldmaps server.

    :param host_url:  URL for the fieldmaps server.
    :param auth_code: fieldmaps server authentication code
    """

    def __init__(self, host_url: str, auth_code: str):
        self.host_url = host_url.rstrip("/")
        self.auth_code = auth_code

    @staticmethod
    def _check_model_str(model_string: str, valid_model_strings: Sequence[str]):
        if model_string not in valid_model_strings:
            raise ValueError(
                f"Model '{model_string}' is not supported. Must be one of {valid_model_strings}."
            )

    @staticmethod
    def _raise_HTTPError(rv: requests.Response):
        """
        Reraise an HTTPError with the requests.response text content.
        """
        try:
            rv.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise requests.exceptions.HTTPError(f"{str(err)}: {rv.text}")

    def _http_get(self, url: str, headers: Dict[str, str]) -> requests.Response:
        logger.debug(f"get: {url} \nwith headers: {headers}")
        rv = requests.get(url, headers=headers)
        if not rv.ok:
            self._raise_HTTPError(rv)
        logger.debug(f"received response {rv.status_code}")
        return rv

    def get(self, fieldmap_filetype: str, key: Union[int, str]) -> requests.Response:
        """Sends a :func:`requests.get` to the fieldmaps server to download an image.

        :param fieldmap_filetype: string designating ORM model
        :param key: The record id from fieldmap_filetype's table for the desired image.
        :return: :class:`requests.Response` object
        """
        self._check_model_str(fieldmap_filetype, fieldmap_filetypes)
        key = int(key)
        url = f"{self.host_url}/api/{fieldmap_filetype}/{key}"
        headers = {"Authorization": self.auth_code}
        return self._http_get(url, headers)

    def _save(self, fieldmap_filetype: str, key: Union[int, str], filehandle: BinaryIO):
        logger.debug(f"requesting {fieldmap_filetype}/{key} from {self.host_url} ...")
        rv = self.get(fieldmap_filetype, key)
        logger.debug("writing response content.")
        filehandle.write(rv.content)

    def save(
        self,
        fieldmap_filetype: str,
        key: Union[int, str],
        path: Union[str, Path],
        overwrite: bool = False,
    ):
        """Save an image to a file.

        :param fieldmap_filetype: string designating ORM model
        :param key: The record id from fieldmap_filetype's table for the desired image.
        :param path: The path to save the image to.
        :param overwrite: overwrite if file exists
        """
        self._check_model_str(fieldmap_filetype, fieldmap_filetypes)
        path = Path(path)
        if (not overwrite) and path.exists():
            raise ValueError(
                f"{path.absolute()} exists. Please choose a different name or use overwrite option."
            )
        if path.suffix != ".h5":
            logger.warning("h5 file is being saved without .h5 extension")
        logger.debug(f"saving file to {path.absolute()}")
        with open(path, "wb") as f:
            self._save(fieldmap_filetype, key, f)

    def count(self, model: str) -> int:
        """Count the number of records in a database table.

        :param model: string designating ORM model
        :return: The number of records in the corresponding database table.
        """
        self._check_model_str(model, [m.__api_name__ for m in get_models()])
        url = f"{self.host_url}/api/{model}/count"
        headers = {"Authorization": self.auth_code}
        rv = self._http_get(url, headers=headers)
        return int(rv.content)

    def info(self, model: str, key: Union[int, str]) -> Dict[str, Any]:
        """Retrieve a dictionary description of a database table record.

        :param model: string designating ORM model
        :param key: The record id from the model table.
        :return: dict with keys equal to table columns and values equal to record values
        """
        self._check_model_str(model, [m.__api_name__ for m in get_models()])
        key = int(key)
        url = f"{self.host_url}/api/{model}/{key}/info"
        headers = {"Authorization": self.auth_code}
        rv = self._http_get(url, headers=headers)
        return json.loads(rv.content)

    def list(self, model: str, after: Union[int, str] = 0) -> List[Dict[str, Any]]:
        """Retrieve a list of up to 500 records from a database table.

        :param model: string designating ORM model
        :param after: The record id after which to start listing.
        :return: list of record descriptions
        """
        self._check_model_str(model, [m.__api_name__ for m in get_models()])
        try:
            after = int(after)
        except (TypeError, ValueError):
            after = None
        url = f"{self.host_url}/api/{model}/list{'/' + str(after) if after else ''}"
        headers = {"Authorization": self.auth_code}
        rv = self._http_get(url, headers=headers)
        return (
            []
            if rv.content == "".encode("utf-8")
            else json.loads(rv.content)["records"]
        )

    def list_all(
        self, model: str, after: int = 0, before: int = np.inf
    ) -> List[Dict[str, Any]]:
        """Retrieve a list of all records from a database table.

        :param model: string designating ORM model
        :param after: The record id after which to start listing.
        :param before: The record id before which to stop listing.
        :return: list of record descriptions
        """
        concatenated_list = []
        result = True
        while result:
            result = self.list(model, after)
            if result:
                after = result[-1]["id"]
            if after >= before - 1:
                result = [x for x in result if x["id"] < before]
                concatenated_list.extend(result)
                break
            concatenated_list.extend(result)
        return concatenated_list

    @staticmethod
    def _record_list_to_structured_array(
        record_list: List[Dict[str, Any]]
    ) -> np.ndarray:
        """convert list of record dicts to structured array"""
        if record_list == []:
            return record_list

        # guess at a map of fieldname to datatype
        dtype_dict = {}
        default_string_type = "<U128"
        server_datetime_type = "datetime64[s]"
        for fieldname, example in record_list[0].items():
            # string must have a predefined length for structured arrays
            if type(example) is str:
                dtype_dict[fieldname] = default_string_type
            else:
                dtype_dict[fieldname] = type(example)

        # update known datetimes and string lengths
        # most string lengths found in fieldmaps web app project fieldmaps/models: FieldmapBase
        # string length for username: flask_cfmm/models: UserMixin
        known_types = {
            "coilname": "<U128",
            "created": server_datetime_type,
            "generated": server_datetime_type,
            "modified": server_datetime_type,
            "path": "<U1024",
            "source": "<U15",
            "system": "<U128",
            "username": "<U255",
        }
        for key in dtype_dict:
            if key in known_types:
                dtype_dict[key] = known_types[key]
        structured_array = np.ndarray(
            shape=[len(record_list)], dtype=[(k, v) for k, v in dtype_dict.items()]
        )
        indx = 0
        for record in record_list:
            # store datetimes in local timezone
            for k, v in record.items():
                if dtype_dict[k] == server_datetime_type:
                    # numpy converts datetime objects to utc, so convert to local timezone and then remove timezone info
                    # this allows the numpy object to be shown in the local timezone
                    record[k] = np.datetime64(
                        dateutil.parser.parse(v).astimezone().replace(tzinfo=None)
                    )
            structured_array[indx] = tuple(record.values())
            indx += 1
        return structured_array

    def list_as_structured_array(
        self, model, after: Union[int, str] = None
    ) -> np.ndarray:
        """Retrieve a structured array of up to 500 records from a database table.

        :param model: string designating ORM model
        :param after: The record id after which to start listing.
        :return: numpy structured array with dtype corresponding to table columns (datetimes are in local timezone)
        """
        return self._record_list_to_structured_array(self.list(model, after))

    def list_all_as_structured_array(
        self, model, after: int = 0, before: int = np.inf
    ) -> np.ndarray:
        """Retrieve a structured array of all records from a database table.

        :param model: string designating ORM model
        :param after: The record id after which to start listing.
        :param before: The record id before which to stop listing.
        :return: numpy structured array with dtype corresponding to table columns (datetimes are in local timezone)
        """
        return self._record_list_to_structured_array(
            self.list_all(model, after, before)
        )

    def _http_post(
        self, url: str, data: Union[bytes, BinaryIO], headers: Dict[str, str] = None
    ) -> requests.Response:
        logger.debug(f"post: {url} \nwith headers: {headers}")
        rv = requests.post(url, data=data, headers=headers)
        if not rv.ok:
            self._raise_HTTPError(rv)

        logger.debug(
            f"received response: {rv.status_code} - {rv.content.decode('utf-8')}"
        )
        return rv

    def register(
        self,
        fieldmap_filetype: str,
        path: Union[str, Path],
        system: str = None,
        coilname: str = None,
    ) -> requests.Response:
        """Sends a :func:`requests.post` to the fieldmaps server to catalog an image already stored on the server.

        :param fieldmap_filetype: string designating ORM model
        :param path: The path on the fieldmap server to the image file
        :param system: The MR system the image was acquired on
        :param coilname: The coil the image was acquired with
        :return: :class:`requests.Response` object
        """
        self._check_model_str(fieldmap_filetype, fieldmap_filetypes)
        headers = {"Authorization": self.auth_code, "Content-Type": "application/json"}
        url = f"{self.host_url}/api/{fieldmap_filetype}/register"
        payload = {
            "path": path,
        }
        if system is not None:
            payload["system"] = system
        if coilname is not None:
            payload["coilname"] = coilname
        data = json.dumps(payload).encode("utf-8")
        return self._http_post(url, data, headers)

    def upload(
        self,
        fieldmap_filetype: str,
        filehandle: BinaryIO,
        system: str = None,
        coilname: str = None,
    ) -> requests.Response:
        """Sends a :func:`requests.post` to the fieldmaps server to catalog the image stored in filehandle.

        :param fieldmap_filetype: string designating ORM model
        :param filehandle: The filehandle for the image
        :param system: The MR system the image was acquired on
        :param coilname: The coil the image was acquired with
        :return: :class:`requests.Response` object
        """
        self._check_model_str(fieldmap_filetype, fieldmap_filetypes)
        headers = {
            "Authorization": self.auth_code,
        }
        if system is not None:
            headers["system"] = system
        if coilname is not None:
            headers["coilname"] = coilname
        url = f"{self.host_url}/api/{fieldmap_filetype}/upload"
        return self._http_post(url, filehandle, headers)

    def upload_path(
        self,
        fieldmap_filetype: str,
        local_path: Union[str, Path],
        system: str = None,
        coilname: str = None,
    ) -> requests.Response:
        """Sends a :func:`requests.post` to the fieldmaps server to catalog the image stored at local_path.

        :param fieldmap_filetype: string designating ORM model
        :param local_path: The path to read the image from
        :param system: The MR system the image was acquired on
        :param coilname: The coil the image was acquired with
        :return: :class:`requests.Response` object
        """

        with open(local_path, "rb") as data:
            return self.upload(fieldmap_filetype, data, system, coilname)

    def modify_file(
        self, fieldmap_filetype: str, key: Union[int, str], filehandle: BinaryIO
    ) -> requests.Response:
        """Sends a :func:`requests.post` to the fieldmaps server to replace image file with given image.

        :param fieldmap_filetype: string designating ORM model
        :param key: The record id from the model table.
        :param filehandle: The filehandle for the image
        :return: :class:`requests.Response` object
        """

        self._check_model_str(fieldmap_filetype, fieldmap_filetypes)
        key = int(key)
        headers = {
            "Authorization": self.auth_code,
        }
        url = f"{self.host_url}/api/{fieldmap_filetype}/{key}/modify_file"
        return self._http_post(url, filehandle, headers)

    def modify_file_with_local_path(
        self, fieldmap_filetype: str, key: Union[int, str], local_path: Union[str, Path]
    ) -> requests.Response:
        """Sends a :func:`requests.post` to the fieldmaps server to replace image file with image at local_path.

        :param fieldmap_filetype: string designating ORM model
        :param key: The record id from the model table.
        :param local_path: The path to read the image from
        :return: :class:`requests.Response` object
        """
        with open(local_path, "rb") as data:
            return self.modify_file(fieldmap_filetype, key, data)

    def _tag(self, model, key, tags, untag):
        self._check_model_str(model, taggable_models)
        key = int(key)
        if isinstance(tags, str):
            tags = [tags]
        prefix = "un" if untag else ""
        url = f"{self.host_url}/api/{model}/{key}/{prefix}tag"
        payload = {"tags": tags}
        headers = {"Authorization": self.auth_code}
        data = json.dumps(payload).encode("utf-8")
        return self._http_post(url, data, headers)

    def tag(self, model: str, key: Union[int, str], tags: Union[str, List[str]]):
        """Tag a database table record with given tag(s).

        :param model: string designating ORM model
        :param key: The record id from the model table.
        :param tags: The tag(s) to give the record.
        """
        return self._tag(model, key, tags, untag=False)

    def untag(self, model: str, key: Union[int, str], tags: Union[str, List[str]]):
        """Remove tag(s) from database table record.

        :param model: string designating ORM model
        :param key: The record id from the model table.
        :param tags: The tag(s) to remove.
        """
        return self._tag(model, key, tags, untag=True)

    def _tag_post(self, tag_endpoint, json_body_dict):
        url = f"{self.host_url}/api/tag/{tag_endpoint}"
        headers = {"Authorization": self.auth_code}
        data = json.dumps(json_body_dict).encode("utf-8")
        return self._http_post(url, data, headers)

    def delete_tags(self, tags: Union[str, List[str]]):
        """Delete a tag from the database and all records.

        :param tags: The tag(s) to delete.
        """
        if isinstance(tags, str):
            tags = [tags]
        payload = {"tags": tags}
        return self._tag_post("delete", payload)

    def rename_tag(self, tag: str, new_name: str):
        """Rename a tag in the database and apply to all tagged records.

        :param tag: The tag to rename.
        :param new_name: The new name.
        """
        payload = {"tag": tag, "new_name": new_name}
        return self._tag_post("rename", payload)

    def merge_tags(self, tags: List[str], merged_name: str):
        """Merge all the given tags into a single tag.

        :param tags: The tags to merge.
        :param merged_name: The single name to give all tags.
        """
        payload = {"tags": tags, "merged_name": merged_name}
        return self._tag_post("merge", payload)

    def list_similar_tags(self, tags: Union[Tuple[str, int], List[Tuple[str, int]]]):
        """List similar tags using fuzzy name matching. In the given (tag, threshold) pair(s), threshold is the
        matching parameter with higher thresholds returning fewer matches. A threshold of 100 only returns tags
        with punctuation differences (eg. human, Human). If the tuple does not include a threshold (eg. (tag,) ),
        then the search will be for an exact match including punctuation.

        :param tags: List of (tag, threshold) pairs where threshold optional and is the fuzzy name matching paarameter.
        """
        if isinstance(tags[0], str):
            tags = [tags]

        # convert tags list to json dicts
        tags_json_dict = []
        for tag_tuple in tags:
            if isinstance(tag_tuple, str):
                tags_json_dict.append({"name": tag_tuple})
            elif len(tag_tuple) == 1:
                tags_json_dict.append({"name": tag_tuple[0]})
            else:
                tags_json_dict.append({"name": tag_tuple[0], "threshold": tag_tuple[1]})
        payload = {"tags": tags_json_dict}
        rv = self._tag_post("similar", payload)
        return (
            []
            if rv.content == "".encode("utf-8")
            else json.loads(rv.content)["matching_tags"]
        )

    def query(
        self,
        model,
        tags: List[Tag] = None,
        coil_ids: List[int] = None,
        generated: Tuple[
            Optional[Union[datetime.datetime, datetime.date]],
            Optional[Union[datetime.datetime, datetime.date]],
        ] = None,
    ):
        """Query model for records satisfying constraints.

        :param model: string designating ORM model
        :param tags: List Tag objects used to build query. Tag objects are combined using their operation attribute.
        :param coil_ids: List of coil ids to query on.
        :param generated: Two tuple containing the `generated` date range to query on.
        :return: list of descriptions of matching records
        """
        payload = {}
        if tags is not None:
            payload["tags"] = [{"tag": tag.json_dict} for tag in tags]
        if coil_ids is not None:
            payload["coils"] = [int(x) for x in coil_ids]
        if generated is not None:
            after, before = generated
            if after is not None:
                if type(after) is datetime.date:
                    after = datetime.datetime.combine(
                        after,
                        datetime.datetime.min.time(),
                        tzinfo=datetime.timezone.utc,
                    )
                if after.tzinfo is None:
                    raise ValueError("after datetime must contain timezone information")
                else:
                    after = after.astimezone(datetime.timezone.utc).isoformat()
            if before is not None:
                if type(before) is datetime.date:
                    before = datetime.datetime.combine(
                        before,
                        datetime.datetime.min.time(),
                        tzinfo=datetime.timezone.utc,
                    )
                if before.tzinfo is None:
                    raise ValueError(
                        "before datetime must contain timezone information"
                    )
                else:
                    before = before.astimezone(datetime.timezone.utc).isoformat()
            payload["generated"] = (after, before)

        url = f"{self.host_url}/api/{model}/query"
        headers = {"Authorization": self.auth_code}
        data = json.dumps(payload).encode("utf-8")
        rv = self._http_post(url, data, headers)
        return (
            []
            if rv.content == "".encode("utf-8")
            else json.loads(rv.content)["matching_records"]
        )

    def query_return_structured_array(
        self,
        model,
        tags: List[Tag] = None,
        coil_ids: List[int] = None,
        generated: Tuple[
            Optional[Union[datetime.datetime, datetime.date]],
            Optional[Union[datetime.datetime, datetime.date]],
        ] = None,
    ) -> np.ndarray:
        """Query model for records satisfying constraints.

        :param model: string designating ORM model
        :param tags: List Tag objects used to build query. Tag objects are combined using their operation attribute.
        :param coil_ids: List of coil ids to query on.
        :param generated: Two tuple containing the `generated` date range to query on.
        :return: matching records in a numpy structured array with dtype corresponding to table columns
        (datetimes are in local timezone)
        """
        return self._record_list_to_structured_array(
            self.query(model, tags, coil_ids, generated)
        )

    def _save_study(
        self,
        study_record,
        save_dir: Union[str, Path],
        overwrite: bool = False,
        study_prefix: str = None,
        b0_prefix: str = None,
        b1_prefix: str = None,
        ptx_prefix: str = None,
    ):
        save_dir = Path(save_dir)
        save_dir.mkdir(parents=True, exist_ok=True)
        study_prefix = "study" if study_prefix is None else study_prefix
        for model in [
            m
            for m in get_models(is_filetype=True)
            if hasattr(m, "study_relationship_prefix")
        ]:
            fieldmap_keys = study_record[f"{model.study_relationship_prefix}_ids"]

            if model == B0:
                type_prefix = model.__api_name__ if b0_prefix is None else b0_prefix
            elif model == B1:
                type_prefix = model.__api_name__ if b1_prefix is None else b1_prefix
            elif model == PTx:
                type_prefix = model.__api_name__ if ptx_prefix is None else ptx_prefix

            for key in fieldmap_keys:
                filepath = (
                    save_dir
                    / f'{study_prefix}_{study_record["id"]}_{type_prefix}_{key}.h5'
                )
                logger.debug(f"saving {filepath}")
                try:
                    self.save(model.__api_name__, key, filepath, overwrite=overwrite)
                except ValueError as e:
                    logger.debug(f"skipping: {str(e)}")

    def save_study(
        self,
        study_key,
        save_dir: Union[str, Path],
        overwrite: bool = False,
        study_prefix: str = None,
        b0_prefix: str = None,
        b1_prefix: str = None,
        ptx_prefix: str = None,
    ):
        """Save image files belonging to a study.

        :param study_key: The study id.
        :param save_dir: The directory to save the images to.
        :param overwrite: overwrite if file exists.
        :param study_prefix: filename study prefix, default is the study api name.
        :param b0_prefix: filename b0 prefix, default is the b0fieldmap api name.
        :param b1_prefix: filename b1 prefix, default is the b1fieldmap api name.
        :param ptx_prefix: filename ptx prefix, default is the ptxpulsedesign api name.
        """
        study_key = int(study_key)
        self._save_study(
            self._record_list_to_structured_array([self.info("study", study_key)])[0],
            save_dir,
            overwrite,
            study_prefix,
            b0_prefix,
            b1_prefix,
            ptx_prefix,
        )

    def query_save(
        self,
        model: str,
        save_dir: Union[str, Path],
        tags: List[Tag] = None,
        coil_ids: List[int] = None,
        generated: Tuple[
            Optional[Union[datetime.datetime, datetime.date]],
            Optional[Union[datetime.datetime, datetime.date]],
        ] = None,
        overwrite: bool = False,
        prefix: str = None,
    ):
        """Save image files satisfying constraints.

        :param model: string designating ORM model.
        :param save_dir: The directory to save the images to.
        :param tags: List Tag objects used to build query. Tag objects are combined using their operation attribute.
        :param coil_ids: List of coil ids to query on.
        :param generated: Two tuple containing the `generated` date range to query on.
        :param overwrite: overwrite if file exists.
        :param prefix: filename prefix, default is the model api name.
        """
        self._check_model_str(model, taggable_models)
        save_dir = Path(save_dir)
        prefix = model if prefix is None else prefix
        records = self.query_return_structured_array(model, tags, coil_ids, generated)
        save_dir.mkdir(parents=True, exist_ok=True)
        for record in records:
            if model in fieldmap_filetypes:
                key = record["id"]
                filepath = save_dir / f"{prefix}_{key}.h5"
                logger.debug(f"saving {filepath}")
                try:
                    self.save(model, key, filepath, overwrite=overwrite)
                except ValueError as e:
                    logger.debug(f"skipping: {str(e)}")
            else:
                self._save_study(record, save_dir, overwrite, study_prefix=prefix)

    def classify(self, model: str, key: Union[int, str], subject: str = None):
        """Classify the model.

        :param model: string designating ORM model.
        :param key: The record id from the model table.
        :param subject: Classify the model with this subject type.
        """
        self._check_model_str(model, taggable_models)
        key = int(key)
        url = f"{self.host_url}/api/{model}/{key}/classify"
        payload = {"subject": subject}
        headers = {"Authorization": self.auth_code}
        data = json.dumps(payload).encode("utf-8")
        return self._http_post(url, data, headers)
