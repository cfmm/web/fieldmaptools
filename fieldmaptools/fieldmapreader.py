import functools
from functools import lru_cache
from pathlib import Path

import h5py
import lxml.etree as ET
import numpy as np
from scipy.interpolate import RegularGridInterpolator

from fieldmaptools.server_models import PTx
from .exceptions import BadFile


def default_error(self, value=None):
    raise NotImplementedError


class FieldmapReader:
    def __init__(self, filepath, mode: str = "r"):
        """

        :param filepath:
        :param mode:
        r        Readonly, file must exist (default)
        r+       Read/write, file must exist
        w        Create file, truncate if exists
        w- or x  Create file, fail if exists
        a        Read/write if exists, create otherwise
        """
        self.filepath = filepath
        self.mode = mode

    @functools.cached_property
    def _hdf5(self):
        try:
            return h5py.File(self.filepath, self.mode)
        except Exception as err:
            raise AttributeError(f"cannot set _hdf5: {str(err)}")

    generatedDateTime = property(default_error, default_error, default_error)
    patientLOID = property(default_error, default_error, default_error)
    patientName = property(default_error, default_error, default_error)
    studyLOID = property(default_error, default_error, default_error)
    systemModel = property(default_error, default_error, default_error)
    transmittingCoil = property(default_error, default_error, default_error)
    protocolName = property(default_error, default_error, default_error)
    fieldmap = property(default_error, default_error, default_error)
    fov = property(default_error, default_error, default_error)
    center = property(default_error, default_error, default_error)

    @property
    def matrix_size(self):
        return np.asarray(self.fieldmap.shape)

    @property
    def resolution(self):
        # voxel resolution where voxel size can include slice thickness + gap
        return self.fov / self.matrix_size[:3]

    @staticmethod
    def get_coord_vectors(fov, center, matrix_size):
        matrix_size = matrix_size[:3]
        return [
            np.linspace(-(fov - res) / 2.0, (fov - res) / 2.0, sz) + offset
            for fov, res, sz, offset in zip(fov, fov / matrix_size, matrix_size, center)
        ]

    @property
    def coord_vectors(self):
        return self.get_coord_vectors(self.fov, self.center, self.matrix_size[:3])

    @property
    def coord_grids(self):
        return np.meshgrid(*self.coord_vectors, indexing="ij")

    def _interpolators(self, img, **kwargs):
        if len(img.shape) == 4:
            imgs = np.split(img, img.shape[-1], axis=-1)
        else:
            imgs = [img]
        coords = self.coord_vectors
        return [RegularGridInterpolator(coords, x, **kwargs) for x in imgs]

    @lru_cache(10)
    def _fieldmap_interpolators(self, **kwargs):
        return self._interpolators(self.fieldmap, **kwargs)

    def interpolate(self, coord_grids, interpolators=None, **kwargs):
        matrix_sz = coord_grids[0].shape
        interpolators = (
            self._fieldmap_interpolators(**kwargs)
            if interpolators is None
            else interpolators
        )
        fieldmaps = [
            interp(tuple(c.ravel() for c in coord_grids)).reshape(matrix_sz)
            for interp in interpolators
        ]
        return np.squeeze(np.stack(fieldmaps, axis=-1))

    def interpolate_zoom(
        self, fov=None, center=None, matrix_size=None, interpolators=None, **kwargs
    ):
        fov = self.fov if fov is None else np.asarray(fov)
        center = self.center if center is None else np.asarray(center)
        matrix_size = (
            self.matrix_size[:3] if matrix_size is None else np.asarray(matrix_size)
        )
        coord_vecs = self.get_coord_vectors(fov, center, matrix_size)
        coord_grids = np.meshgrid(*coord_vecs, indexing="ij")
        try:
            return self.interpolate(coord_grids, interpolators=interpolators, **kwargs)
        except ValueError as e:
            raise ValueError(
                f"{e}\n\nif above is out of bounds error, try including keywords "
                "`bounds_error=False,fill_value=0` (all keywords are passed to the interpolator)."
            )

    def _get_hdf5_path(self, key_list):
        # slower but indicates exactly which part of the path is missing
        obj = self._hdf5
        key = None
        try:
            for key in key_list:
                obj = obj[key]
        except (ValueError, KeyError):
            if key:
                msg = "/".join(key_list[: key_list.index(key) + 1])
            else:
                msg = ""
            raise AttributeError(f"hdf5 path '/{msg}' does not exist in file.")
        return obj

    @classmethod
    def file(cls, path: Path, mode: str = None):
        """
        :param path:
        :param mode:
            r        Readonly, file must exist (default)
            r+       Read/write, file must exist
            w        Create file, truncate if exists
            w- or x  Create file, fail if exists
            a        Read/write if exists, create otherwise
        :return:
        """

        return cls(path, mode)

    @property
    def _mapcapture_maptypes(self):
        # only works for mapcapture files
        maptypes = set()
        if "images/B1 Magnitude" in self._hdf5:
            maptypes.add("b1")
        if "images/B0 Map" in self._hdf5:
            maptypes.add("b0")

        try:
            root = ET.XML(self._hdf5["images/Raw Data/attributes"][0])
            for value in root.findall("meta[name='Map Type']/value"):
                # use lower to match with api name
                maptypes.add(value.text.lower())
        except KeyError as err:
            if not maptypes:
                raise AttributeError(f"maptypes: {str(err)}")

        return maptypes

    @property
    def filetypes(self):
        # mapcapture files
        try:
            return self._mapcapture_maptypes
        except AttributeError:
            pass
        # ptxpulsedesign files
        if "/Adj" in self._hdf5:
            return {PTx.__api_name__}
        return set()

    def check_file(self):
        try:
            # Get the cached hdf5 file, if not already loaded.
            # noinspection PyStatementEffect
            self._hdf5
        except AttributeError as err:
            raise BadFile(str(err))

    def flush(self):
        return self._hdf5.flush()

    def close(self):
        return self._hdf5.close()

    def create_hdf5(self):
        tmp = h5py.File(self.filepath, "w")
        tmp.close()
