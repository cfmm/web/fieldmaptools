import inspect
import sys


class PropertyMixin:
    is_filetype = False
    is_taggable = False


class B1(PropertyMixin):
    __api_name__ = "b1"
    is_filetype = True
    is_taggable = True
    study_relationship_prefix = "b1"


class B0(PropertyMixin):
    __api_name__ = "b0"
    is_filetype = True
    is_taggable = True
    study_relationship_prefix = "b0"


class PTx(PropertyMixin):
    __api_name__ = "ptxpulsedesign"
    is_filetype = True
    is_taggable = True
    study_relationship_prefix = "ptx"


class Study(PropertyMixin):
    __api_name__ = "study"
    is_taggable = True


class Tag(PropertyMixin):
    __api_name__ = "tag"


class Coil(PropertyMixin):
    __api_name__ = "coil"


def get_models(**kwargs):
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj) and getattr(obj, "__api_name__", False):
            if kwargs:
                if all(
                    (hasattr(obj, attr) and getattr(obj, attr) == val)
                    for attr, val in kwargs.items()
                ):
                    yield obj
            else:
                yield obj
