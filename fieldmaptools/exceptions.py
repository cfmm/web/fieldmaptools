class BadFile(Exception):
    pass


class IncompleteData(BadFile):
    pass


class IncorrectXML(BadFile):
    pass
