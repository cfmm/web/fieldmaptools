"""
reads files created by: https://git.cfmm.uwo.ca/idea/b1capture
"""

import datetime
import functools
from functools import lru_cache

import dateutil.parser
import lxml.etree as ET
import numpy as np

from .exceptions import IncorrectXML, IncompleteData
from .fieldmapreader import FieldmapReader, default_error


def get_element(self, element_search_list):
    error_msg = ""
    for element_tag, use_namespace in element_search_list:
        results = self._xml.findall(
            f".//*{element_tag}", self._xml.nsmap if use_namespace else None
        )
        if len(results) == 1:
            # this function only supports tags that return exactly one element from the xml
            return results[0]
        else:
            error_msg += f"{len(results)} tags found for {element_tag}, "
    raise AttributeError(f"{error_msg}in Fieldmap file's xml.")


def xml_getter(self, element_search_list):
    return getattr(get_element(self, element_search_list), "text")


def xml_setter(self, value, element_search_list):
    setattr(get_element(self, element_search_list), "text", str(value))
    # h5py writes to the file with each setattr, so this will modify the actual file
    self._hdf5_xml[0] = ET.tostring(self._xml, encoding="UTF-8", xml_declaration=True)


def make_xml_property(element_search_list):
    def getter(self):
        return xml_getter(self, element_search_list)

    def setter(self, value):
        xml_setter(self, value, element_search_list)

    return property(getter, setter)


class MapCaptureReader(FieldmapReader):

    @functools.cached_property
    def _hdf5_xml(self):
        return self._get_hdf5_path(["images", "xml"])

    @functools.cached_property
    def _xml(self):
        return ET.XML(self._hdf5_xml[0])

    @functools.cached_property
    def image_version(self):
        return self._get_hdf5_path(["images", "B1 Magnitude", "header", 0, 0])

    @functools.cached_property
    def header_version(self):
        tmp = self._xml.find("version", self._xml.nsmap)
        if tmp:
            return tmp.text
        return None

    @property
    def generatedDateTime(self):
        date_string = xml_getter(
            self, [("siemens/Custom/DateTime", False), ("ProcessingDateTime", True)]
        )
        try:
            datetime_obj = dateutil.parser.parse(date_string)
        except dateutil.parser.ParserError:
            try:
                datetime_obj, ignored = dateutil.parser.parse(
                    date_string, fuzzy_with_tokens=True
                )
                if "eastern daylight time" in [x.strip().lower() for x in ignored]:
                    datetime_obj = datetime_obj.replace(
                        tzinfo=datetime.timezone(datetime.timedelta(hours=-4))
                    )
                elif "eastern standard time" in [x.strip().lower() for x in ignored]:
                    datetime_obj = datetime_obj.replace(
                        tzinfo=datetime.timezone(datetime.timedelta(hours=-5))
                    )
                else:
                    raise AttributeError(
                        f"DateTime string '{date_string}' has elements {ignored} not understood."
                    )
            except Exception as err:
                raise AttributeError(f"generatedDateTime: {str(err)}")
        return datetime_obj

    @generatedDateTime.setter
    def generatedDateTime(self, value):
        if value.utcoffset() is None:
            value = value.astimezone()
        value = value.isoformat(sep=" ")
        xml_setter(
            self,
            value,
            [("siemens/Custom/DateTime", False), ("ProcessingDateTime", True)],
        )

    patientLOID = make_xml_property(
        [("siemens/HEADER/PatientLOID", False), ("patientLOID", True)]
    )
    patientName = make_xml_property(
        [("siemens/HEADER/tPatientName", False), ("patientName", True)]
    )
    studyLOID = make_xml_property(
        [("siemens/HEADER/StudyLOID", False), ("studyLOID", True)]
    )
    systemModel = make_xml_property(
        [("siemens/DICOM/ManufacturersModelName", False), ("systemModel", True)]
    )
    transmittingCoil = make_xml_property(
        [("siemens/YAPS/tTransmittingCoil", False), ("TransmittingCoil", True)]
    )
    protocolName = make_xml_property([("protocolName", True), ("ProtocolName", True)])
    rawData = property(default_error, default_error, default_error)

    @property
    def maptypes(self):
        return self._mapcapture_maptypes

    @property
    def _rawData_native(self):
        try:
            # every time you access the h5py Dataset's getter, it returns a new copy of the array
            # which does not affect the original dataset version
            real = self._get_hdf5_path(["images", "Raw Data", "data", "real"])
            imag = self._get_hdf5_path(["images", "Raw Data", "data", "imag"])
        except AttributeError as err:
            raise AttributeError(f"_rawData_native: {str(err)}")
        return real + 1j * imag

    def check_file(self):
        super().check_file()
        # try to access data to check for aborted acquisitions
        try:
            data = self._rawData_native
            # check for all zeroes
            if np.array_equal(data, np.zeros_like(data)):
                raise ValueError("Raw Data is all zeroes.")
        except (AttributeError, ValueError) as err:
            raise IncompleteData(str(err))
        # check for missing XML
        try:
            self._hdf5_xml
        except AttributeError:
            raise IncorrectXML

    @staticmethod
    def _reformat_img(img, nsets):
        # B1: freq,phase,slice,coil
        # B0: freq,phase,slice,echo
        img = np.squeeze(img).transpose()
        img = img.reshape(img.shape[:-1] + (-1, nsets))
        img = np.squeeze(img)
        return img

    @property
    def slice_fov(self):
        return self._get_hdf5_path(["images", "Raw Data", "header", "field_of_view", 0])

    @property
    def slice_resolution(self):
        return self.slice_fov / np.append(self.matrix_size[:2], 1)

    @property
    def center(self):
        return (
            self._get_hdf5_path(["images", "Raw Data", "header", "position"]).max(
                axis=0
            )
            + self._get_hdf5_path(["images", "Raw Data", "header", "position"]).min(
                axis=0
            )
        ) / 2

    @property
    def fov(self):
        # volume fov is (voxel resolution * matrix size)
        # note that voxel resolution and volume fov include slice thickness + gap size
        in_plane_fov = self.slice_fov[:2]
        slice_posns = list(
            set(
                self._get_hdf5_path(["images", "Raw Data", "header", "position"])[:, -1]
            )
        )
        slice_posns.sort()
        slice_dir_fov = (slice_posns[-1] - slice_posns[0]) + (
            slice_posns[1] - slice_posns[0]
        )
        return np.append(in_plane_fov, slice_dir_fov)

    @lru_cache(10)
    def _rawdata_mag_interpolators(self, **kwargs):
        # problems occur with complex interpolation because Siemens images contain phase singularities
        return self._interpolators(np.abs(self.rawData), **kwargs)

    def interpolate_rawdata_mag(self, coord_grids, **kwargs):
        return self.interpolate(
            coord_grids, interpolators=self._rawdata_mag_interpolators(**kwargs)
        )

    def interpolate_zoom_rawdata_mag(
        self, fov=None, center=None, matrix_size=None, **kwargs
    ):
        return self.interpolate_zoom(
            fov=fov,
            center=center,
            matrix_size=matrix_size,
            interpolators=self._rawdata_mag_interpolators(**kwargs),
        )
