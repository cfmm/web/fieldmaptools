import numpy as np
from copy import deepcopy

from .exceptions import IncompleteData
from .mapcapturereader import MapCaptureReader


class B1Reader(MapCaptureReader):

    @property
    def ncoils(self):
        return (
            self._get_hdf5_path(["images", "B1 Magnitude", "header", "set"]).max() + 1
        )

    @property
    def _fieldmap_native(self):
        try:
            # every time you access the h5py Dataset's getter, it returns a new copy of the array
            # which does not affect the original dataset version
            mag_array = self._get_hdf5_path(["images", "B1 Magnitude", "data"])[:]
            phase_array = self._get_hdf5_path(["images", "B1 Phase", "data"])[:]
        except AttributeError as err:
            raise AttributeError(f"_fieldmap_native: {str(err)}")
        return mag_array * np.exp(1j * phase_array)

    @property
    def _mask_native(self):
        try:
            # every time you access the h5py Dataset's getter, it returns a new copy of the array
            # which does not affect the original dataset version
            mask_array = self._get_hdf5_path(["images", "Mask", "data"])[:]
        except AttributeError as err:
            raise AttributeError(f"_mask_native: {str(err)}")
        return mask_array

    @property
    def rawData(self):
        try:
            nvolumes = self._rawData_native.shape[0] // self.fieldmap.shape[2]
            tmp = self._reformat_img(self._rawData_native, nvolumes)
            sort_indices = np.argsort(
                self._hdf5["images"]["Raw Data"]["header"]["image_index"][::nvolumes]
            )
            return tmp[..., sort_indices, :]
        except AttributeError as err:
            raise AttributeError(f"rawData: {str(err)}")

    @property
    def fieldmap(self):
        try:
            return self._reformat_img(self._fieldmap_native, self.ncoils)
        except AttributeError as err:
            raise AttributeError(f"fieldmap: {str(err)}")

    @staticmethod
    def transpose_rotation(img, rot_matrix):
        for i in range(2):
            assert (
                np.sum(np.abs(rot_matrix[i, :]) == 1) == 1
            ), "Invalid rotation matrix!"
        indx_transpose = []
        indx_transpose.append(np.where(np.abs(rot_matrix[0, :]))[0][0])
        indx_transpose.append(np.where(np.abs(rot_matrix[1, :]))[0][0])
        indx_transpose.append(np.where(np.abs(rot_matrix[2, :]))[0][0])

        # used for 1d arrays descriptive arrays like shape, fov, center, matrix_size
        if img.ndim == 1:
            retval = deepcopy(img)
            retval[:3] = retval[indx_transpose]
            return retval

        # used for ndarrays, but only rotate in 3d
        indx_transpose += list(range(3, img.ndim))
        img = img.transpose(indx_transpose)
        if np.any(rot_matrix[0, :] < 0):
            img = img[::-1, :, :]
        if np.any(rot_matrix[1, :] < 0):
            img = img[:, ::-1, :]
        if np.any(rot_matrix[2, :] < 0):
            img = img[:, :, ::-1]
        return img

    @property
    def pcs_rot_mat(self):
        rot_mat = np.empty((3, 3))
        rot_mat[:, 0] = self._hdf5["images"]["B1 Magnitude"]["header"]["read_dir"][0]
        rot_mat[:, 1] = self._hdf5["images"]["B1 Magnitude"]["header"]["phase_dir"][0]
        rot_mat[:, 2] = self._hdf5["images"]["B1 Magnitude"]["header"]["slice_dir"][0]
        return rot_mat

    @property
    def fieldmap_pcs(self):
        # does not yet support generalized rotations
        return self.transpose_rotation(self.fieldmap, self.pcs_rot_mat)

    @property
    def mask(self):
        try:
            return self._reformat_img(self._mask_native, 1)
        except AttributeError as err:
            raise AttributeError(f"mask: {str(err)}")

    def check_file(self):
        try:
            super().check_file()
        except IncompleteData:
            # old B1 maps do not have /images/Raw Data
            pass
        try:
            data = self._fieldmap_native
            # check for all zeroes
            if np.array_equal(data, np.zeros_like(data)):
                raise ValueError("B1 Map is all zeroes.")
        except (AttributeError, ValueError) as err:
            raise IncompleteData(str(err))

    @property
    def slice_fov(self):
        return self._get_hdf5_path(
            ["images", "B1 Magnitude", "header", "field_of_view", 0]
        )

    @property
    def center(self):
        return (
            self._get_hdf5_path(["images", "B1 Magnitude", "header", "position"]).max(
                axis=0
            )
            + self._get_hdf5_path(["images", "B1 Magnitude", "header", "position"]).min(
                axis=0
            )
        ) / 2

    @property
    def fov(self):
        # volume fov is (voxel resolution * matrix size)
        # note that voxel resolution and volume fov include slice thickness + gap size
        in_plane_fov = self.slice_fov[:2]
        slice_index = self._hdf5["images"]["B1 Magnitude"]["header"]["slice_dir"][
            0
        ].astype("bool")
        slice_posns = list(
            set(
                self._get_hdf5_path(["images", "B1 Magnitude", "header", "position"])[
                    :, slice_index
                ].ravel()
            )
        )
        slice_posns.sort()
        slice_dir_fov = (slice_posns[-1] - slice_posns[0]) + (
            slice_posns[1] - slice_posns[0]
        )
        return np.append(in_plane_fov, slice_dir_fov)

    def interpolate_to_match(
        self, other, other_fov=None, other_c=None, other_matrix_sz=None
    ):
        other_fov = other.fov.copy() if other_fov is None else np.asarray(other_fov)
        other_c = other.center.copy() if other_c is None else np.asarray(other_c)
        other_matrix_sz = (
            other.matrix_size.copy()
            if other_matrix_sz is None
            else np.asarray(other_matrix_sz)
        )

        # put other's native space coordinate descriptors into this fieldmap's native space
        # first to pcs orientation
        other_fov = other.transpose_rotation(other_fov, other.pcs_rot_mat)
        other_c = other.transpose_rotation(other_c, other.pcs_rot_mat)
        other_matrix_sz = other.transpose_rotation(other_matrix_sz, other.pcs_rot_mat)
        # then from pcs to this fieldmap's native space
        pcs_inv = np.linalg.inv(self.pcs_rot_mat)
        other_fov = other.transpose_rotation(other_fov, pcs_inv)
        other_c = other.transpose_rotation(other_c, pcs_inv)
        other_matrix_sz = other.transpose_rotation(other_matrix_sz, pcs_inv)

        # other's coordinate grid in this fieldmap's native space
        coord_vecs = self.get_coord_vectors(other_fov, other_c, other_matrix_sz[:3])
        coord_grids = np.meshgrid(*coord_vecs, indexing="ij")
        match_other = self.interpolate(coord_grids, bounds_error=False, fill_value=0)

        # now fix the interpolated image's orientation from our native space to the other's native space
        match_other = self.transpose_rotation(match_other, self.pcs_rot_mat)
        match_other = self.transpose_rotation(
            match_other, np.linalg.inv(other.pcs_rot_mat)
        )
        return match_other
