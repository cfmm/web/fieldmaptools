from dataclasses import dataclass, field

from fieldmaptools.b0reader import B0Reader
from fieldmaptools.b1reader import B1Reader
from fieldmaptools.fieldmapreader import FieldmapReader
from fieldmaptools.ptxpulsedesignreader import PtxPulseDesignReader

from .server_models import B0, B1, PTx


class OpBase:
    and_ = "AND"
    or_ = "OR"


@dataclass
class Op:
    op: OpBase
    negate: bool = False

    @property
    def json_dict(self):
        return {"operator": self.op, "negate": str(self.negate).lower()}


@dataclass
class Tag:
    name: str
    threshold: int = 0
    op: Op = field(default_factory=lambda: Op(OpBase.or_))

    @property
    def json_dict(self):
        return {
            "name": self.name,
            "threshold": self.threshold,
            "operation": self.op.json_dict,
        }


def get_reader_class(model_api_name):
    if model_api_name == B0.__api_name__:
        return B0Reader
    elif model_api_name == B1.__api_name__:
        return B1Reader
    elif model_api_name == PTx.__api_name__:
        return PtxPulseDesignReader
    else:
        raise ValueError(f"filetype {model_api_name} not understood")


def get_reader(path, *args, **kwargs):
    filetype = kwargs.pop("filetype", None)

    filetypes = FieldmapReader(path).filetypes
    if filetype is None:
        if B1.__api_name__ in filetypes:
            filetype = B1.__api_name__
        elif B0.__api_name__ in filetypes:
            filetype = B0.__api_name__
        elif PTx.__api_name__ in filetypes:
            filetype = PTx.__api_name__
        else:
            try:
                filetype = filetypes.pop()
            except KeyError:
                return None
    elif filetype not in filetypes:
        return None

    reader_class = get_reader_class(filetype)
    if reader_class is not None:
        return reader_class(path, *args, **kwargs)

    return None
