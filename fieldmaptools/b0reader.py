import numpy as np
from libang import unwrap, freq_est

from .mapcapturereader import MapCaptureReader


class B0Reader(MapCaptureReader):
    @property
    def te(self):
        te = (
            np.array(
                [
                    float(elem.text)
                    for elem in self._xml.findall(".//*TE", self._xml.nsmap)
                ]
            )
            * 1e-3
        )  # s
        te.sort()
        return te

    @property
    def nechoes(self):
        return (
            self._get_hdf5_path(["images", "Raw Data", "header", "contrast"]).max() + 1
        )

    @property
    def rawData(self):
        return self._reformat_img(self._rawData_native, self.nechoes)

    def calculate_fieldmap(self, subtract_echo1=True):
        raw_data = self.rawData
        te = self.te

        # least squares weights should be proportional to 1/variance.
        # phase stdv is proportional to 1/SNR.
        # therefore, phase variance is proportional to 1 / mag^2 and weights = mag^2
        if subtract_echo1:
            te = te[1:] - te[0]
            phase_data = np.angle(raw_data[..., 1:] * raw_data[..., :1].conj())
            indx = 1
        else:
            phase_data = np.angle(raw_data)
            indx = 0

        weights = np.abs(raw_data[..., indx:]) ** 2

        # spatial temporal unwrap
        unwrapped = unwrap.spatial_temporal(phase_data, te, weight=weights)
        freq_hz = freq_est.lwlsq(unwrapped, te, weight=weights) / (2 * np.pi)
        return freq_hz

    @property
    def cfmm_fieldmap(self):
        try:
            # every time you access the h5py Dataset's getter, it returns a new copy of the array
            # which does not affect the original dataset version
            return self._get_hdf5_path(["images", "B0 Map"])[:]
        except AttributeError as err:
            raise AttributeError(f"fieldmap: {str(err)}")

    @cfmm_fieldmap.setter
    def cfmm_fieldmap(self, img):
        try:
            data = self._get_hdf5_path(["images", "B0 Map"])
            data[...] = img
        except AttributeError:
            self._hdf5["images"].create_dataset("B0 Map", data=img)
        self.flush()

    @property
    def fieldmap(self):
        try:
            return self._reformat_img(
                self._get_hdf5_path(["images", "B0 Map", "data"]), 1
            )
        except AttributeError:
            pass
        return self.cfmm_fieldmap
