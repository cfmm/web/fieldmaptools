.. fieldmaptools documentation master file, created by
   sphinx-quickstart on Mon Mar 29 14:15:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fieldmaptools's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   API <_modules/modules>
   LICENSE







Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
