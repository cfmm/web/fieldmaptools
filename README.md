## fieldmaptools

Tools for accessing and processing fieldmaps

## Install
requires python >= 3.8, using a virtual environment is recommended
```
pip install fieldmaptools --extra-index-url https://gitlab.com/api/v4/projects/24717924/packages/pypi/simple
```

## Matlab usage
```
% change matlab's default python interpreter (you only need to do this once)
% https://www.mathworks.com/help/matlab/ref/pyenv.html
% ensure the execution mode is OutOfProcess
% ensure you're using a version of matlab that supports your python version
% https://www.mathworks.com/content/dam/mathworks/mathworks-dot-com/support/sysreq/files/python-compatibility.pdf
pyenv('Version','<virtualenv path>/bin/python', 'ExecutionMode','OutOfProcess')

% create authorized client
import py.fieldmaptools.FieldmapsClient
client = FieldmapsClient('https://fieldmap.cfmm.uwo.ca', <your auth code as a string>);

% call a fieldmaptools python function to retrieve study records
study_desc_array = client.list_all_as_structured_array('study');

% alternatively can search based on coil id and tags
% first match desired coilname to coil id
py.eval("coil_desc_array[['coilname','id']]", py.dict(pyargs('coil_desc_array',client.list_all_as_structured_array('coil'))))
% then create query using appropriate coil id and tag (eg. studies acquired using coil with id 2 and tagged as human) 
import py.fieldmaptools.helpers.Tag
study_desc_array = client.query_return_structured_array('study',pyargs('tags',{Tag('human')},'coil_ids',{2}));

% convert python structured array columns to matlab table columns
study_table = table;
study_table.id = cellfun(@int64,cell(py.eval("a['id'].tolist()",py.dict(pyargs('a',study_desc_array))))');
study_table.generated = cellfun(@(x) datetime(char(x.isoformat())),cell(py.eval("a['generated'].tolist()",py.dict(pyargs('a',study_desc_array))))');
study_table.studyloid = cellfun(@char,cell(py.eval("a['studyloid'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.patientloid = cellfun(@char,cell(py.eval("a['patientloid'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.coilname = cellfun(@char,cell(py.eval("a['coilname'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.tags = cellfun(@(py_tag_list) cellfun(@char,cell(py_tag_list),'UniformOutput',false),cell(py.eval("a['tags'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.tags_print = cellfun(@char,cell(py.eval("a['tags'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.b1_ids = cellfun(@(py_id_list) cellfun(@int64,cell(py_id_list)),cell(py.eval("a['b1_ids'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.b0_ids = cellfun(@(py_id_list) cellfun(@int64,cell(py_id_list)),cell(py.eval("a['b0_ids'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
study_table.ptx_ids = cellfun(@(py_id_list) cellfun(@int64,cell(py_id_list)),cell(py.eval("a['ptx_ids'].tolist()",py.dict(pyargs('a',study_desc_array))))','UniformOutput',false);
disp(study_table)

% matlab table search
has_all_imgs = false(size(study_table,1));
for k = 1:size(study_table,1)
  has_all_imgs(k) = (~isempty(study_table(k,:).b1_ids{1}) & ~isempty(study_table(k,:).b0_ids{1}) & ~isempty(study_table(k,:).ptx_ids{1}));
end
after_date = study_table.generated>datetime(2021,1,1);
before_date = study_table.generated<datetime(2021,2,1);
indx = has_all_imgs & after_date & before_date;
study_table(indx, :)

% retrieve b1 records
b1_desc_array = client.list_all_as_structured_array('b1');
b1_table=table;
b1_table.id = cellfun(@int64,cell(py.eval("a['id'].tolist()",py.dict(pyargs('a',b1_desc_array))))');
b1_table.generated = cellfun(@(x) datetime(char(x.isoformat())),cell(py.eval("a['generated'].tolist()",py.dict(pyargs('a',b1_desc_array))))');
b1_table.coilname = cellfun(@char,cell(py.eval("a['coilname'].tolist()",py.dict(pyargs('a',b1_desc_array))))','UniformOutput',false);
b1_table.tags = cellfun(@(py_tag_list) cellfun(@char,cell(py_tag_list),'UniformOutput',false),cell(py.eval("a['tags'].tolist()",py.dict(pyargs('a',b1_desc_array))))','UniformOutput',false);
b1_table.tags_print = cellfun(@char,cell(py.eval("a['tags'].tolist()",py.dict(pyargs('a',b1_desc_array))))','UniformOutput',false);
b1_table.study_id = cellfun(@int64,cell(py.eval("[s['id'] for s in a['study']]",py.dict(pyargs('a',b1_desc_array))))');
b1_table.error = cellfun(@logical,cell(py.eval("a['error'].tolist()",py.dict(pyargs('a',b1_desc_array))))');
disp(b1_table)

% search b1 by study_id
search_results = b1_table(b1_table.study_id==318, :)

% download search results
for i = 1:size(search_results.id)
b1_id = search_results.id(i);
client.save('b1',b1_id,sprintf('b1_%d.h5',b1_id));
end

% tag an image (eg. tag b0 with id 3 as 'distorted')
client.tag('b0', 3, 'distorted')
% untag an image
client.untag('b0', 3, 'distorted')

% search based on tags
import py.fieldmaptools.helpers.Tag
import py.fieldmaptools.helpers.Op
% eg. search all distorted b0 images acquired using coil 2
distorted_b0 = client.query_return_structured_array('b0',pyargs('tags',{Tag('distorted')},'coil_ids',{2}));
% eg. search all b0 images acquired using coil 2 that are not distorted
undistorted_b0 = client.query_return_structured_array('b0',pyargs('tags',{Tag('distorted',pyargs('op',Op('AND',pyargs('negate',true))))},'coil_ids',{2}));
% eg. search all b0 images acquired using coil 2 that are not distorted and are also tagged 'ak_class_1'
c1_undistorted_b0 = client.query_return_structured_array('b0',pyargs('tags',{Tag('distorted',pyargs('op',Op('AND',pyargs('negate',true)))),Tag('ak_class_1',pyargs('op',Op('AND')))},'coil_ids',{2}));
```